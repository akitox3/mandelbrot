#ifndef COMPLEXNUMBER_HPP
#define COMPLEXNUMBER_HPP

class ComplexNumber{
public:
	double ReTeil;
	double ImTeil;
	ComplexNumber(double y, double x);
	ComplexNumber(ComplexNumber *complex);
	void Square();
	ComplexNumber operator+(ComplexNumber other);
	ComplexNumber operator*(ComplexNumber other);
	bool operator==(ComplexNumber other);
};

#endif
