#include "ComplexNumber.hpp"

double ReTeil;
double ImTeil;

ComplexNumber::ComplexNumber(double imaginaer, double real){
	this->ImTeil = imaginaer;
	this->ReTeil = real;
}

ComplexNumber::ComplexNumber(ComplexNumber *complex){
	this->ImTeil = complex->ImTeil;
	this->ReTeil = complex->ReTeil;
}

void ComplexNumber::Square(){
	//c� = (RT1 + i*IM1) * (RT2 + i*IM2)
	//= (RT1 * RT2 + -1*IT1 * IT2) + (RT1 * IT2 + IT1 * RT2)
	//= (RT1 * RT2 - IT1 * IT2) + (RT1 * IT2 + IT1 * RT2)
	double reTeil = this->ReTeil, imTeil = this->ImTeil;
	this->ReTeil = reTeil * reTeil - imTeil * imTeil;
	this->ImTeil = reTeil * imTeil + imTeil * reTeil;

}

ComplexNumber ComplexNumber::operator+(ComplexNumber other){
	ComplexNumber ret(this->ImTeil + other.ImTeil, this->ReTeil + other.ReTeil);
	return ret;
}

bool ComplexNumber::operator==(ComplexNumber other){
	if(this->ImTeil == other.ImTeil && this->ReTeil == other.ReTeil)
		return true;
	else
		return false;
}

ComplexNumber ComplexNumber::operator*(ComplexNumber other){
	double reTeil = ReTeil, imTeil = ImTeil;
	double reTeil2 = other.ReTeil, imTeil2 = other.ImTeil;
	return new ComplexNumber(reTeil * imTeil2 + imTeil * reTeil2, reTeil * reTeil2 - imTeil * imTeil2);
}